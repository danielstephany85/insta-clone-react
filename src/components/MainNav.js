import React, {Component} from 'react';
import { NavLink } from 'react-router-dom'

class MainNav extends Component {

    render = () => {
        const profileLink = (
            <NavLink className='nav-icon' to="/posts">
                <i className="fa fa-user" aria-hidden="true"></i>
            </NavLink>
        );

        return(
        <nav className='main-nav'>
            <div className="main-nav__container">
                <NavLink to="/posts" className="main-logo">
                    <div className="main-logo__img">
                        <i className="fa fa-instagram" aria-hidden="true"></i>
                    </div>
                    <h1 className="main-logo__name">Instagram</h1>
                </NavLink>
             
                <div className='nav-icons'>
                        {(this.props.isRoot ? "" : profileLink)}
                </div>
            </div>
        </nav>
    );
    }
    
} 

export default MainNav;