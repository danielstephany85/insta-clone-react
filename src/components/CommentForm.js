import React, {Component} from 'react';
import PropTypes from 'prop-types';

class CommentForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            commentValue: ''
        }
    }

    static propTypes = {
        parentIndex: PropTypes.number.isRequired,
        addComment: PropTypes.func.isRequired
    }

    commentId = 1000;

    creatComment = () => {
        this.commentId++;
        return {
            id: this.commentId+"",
            comment: this.state.commentValue,
            posted_by: "User"
        };
    }

    handleSubmit = (e) => {
        e.preventDefault(e);
        var comment = this.creatComment();
        this.props.addComment(this.props.parentIndex, comment);
        this.setState({ commentValue: ''}); 
    }

    render = () => {
        return(
            <section className="post-card-new-comment">
                <form action="" onSubmit={(e)=>{this.handleSubmit(e)}}>
                    <input type="text" placeholder="Add a comment..." value={this.state.commentValue} onChange={(e)=>{this.setState({commentValue: e.target.value})}} />
                </form>
            </section>
        );
    }
}

export default CommentForm;