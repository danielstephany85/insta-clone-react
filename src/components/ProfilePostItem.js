import React from 'react';
import PropTypes from 'prop-types';

const ProfilePostItem = (props) => {

    let iconClass;
    switch(props.type){
        case 'carousel':
            iconClass = 'has-slides';
            break;
        case 'video':
            iconClass = 'is-video';
            break;
        default:
            iconClass = '';
    }

    return(
        <div className="post-item-container" ref={(div)=> {this.postItem = div}} onClick={()=>props.showPost(props.index, props.id)}>
            <div className={"post-item " + iconClass} style={{backgroundImage: 'url('+props.imgUrl+')'}}>
                <div className="post-item__info">
                    <span><i className="fa fa-heart" aria-hidden="true"></i>{props.likes}</span>
                    <span><i className="fa fa-comment" aria-hidden="true"></i>{props.comments}</span>
                </div>
            </div>
        </div>
    );
}

ProfilePostItem.propTypes = {
    imgUrl: PropTypes.string.isRequired,
    likes: PropTypes.number.isRequired,
    comments: PropTypes.number.isRequired,
    type: PropTypes.string,
    id: PropTypes.string,
    index: PropTypes.number.isRequired,
    showPost: PropTypes.func.isRequired
}

export default ProfilePostItem;

