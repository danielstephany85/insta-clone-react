import React, { Component } from 'react';
import ProfileInfo from './ProfileInfo';
import PostViewContainer from './PostViewContainer';
import PropTypes from 'prop-types';

class UserProfile extends Component {

    showPost = (index, id) => {
        if (window.innerWidth <= 700) {
            // add a / to change the root path, otherwise path is appended onto the current root
            let post_path = `/post/${id}/${index}`;
            this.props.history.push(post_path);
        }
        else {
            // use passed function from app to set overlay state
            this.props.toggleOverlay(id, index);
        }
    }

    render = () =>{
        return (
            <div className="main-content">
                {(this.props.userData === undefined) ? <p>Loading...</p> : <ProfileInfo userData={this.props.userData} />}
                <PostViewContainer match={this.props.match} postData={this.props.postData} showPost={this.showPost} />
            </div>
        );
    }
}

UserProfile.propTypes = {
    postData: PropTypes.array,
    userData: PropTypes.object,
    toggleOverlay: PropTypes.func
}

export default UserProfile;