import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Comment extends Component {
    render = () => {
        return(
            <li>
                <button onClick={() => { this.props.deleteComment(this.props.index,this.props.id)}}>
                    <i className="fa fa-times" aria-hidden="true"></i>
                </button>
                <span><a>{this.props.postedBy}</a> {this.props.comment}</span>
            </li>
        );
    }
}

Comment.propTypes = {
    comment: PropTypes.string.isRequired,
    postedBy: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    deleteComment: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired
}