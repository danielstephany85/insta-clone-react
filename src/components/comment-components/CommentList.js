import React from 'react';
import Comment from './Comment';
import PropTypes from 'prop-types';

const CommentList = (props) => {

    const Comments = props.comments.map((comment,i)=> {
        return (
            <Comment 
                key={i}
                comment={comment.comment} 
                index={props.parentIndex} 
                id={comment.id}
                postedBy={comment.posted_by} 
                deleteComment={props.deleteComment}
            />
        );
    });

    return(
        <section className='post-card-comments'>
            <ul>
              {Comments}
            </ul>
        </section>
    );
}

CommentList.propTypes = {
    comments: PropTypes.array.isRequired,
    parentIndex: PropTypes.number.isRequired,
    deleteComment: PropTypes.func.isRequired
}

export default CommentList;