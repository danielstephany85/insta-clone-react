import React from 'react';
import ProfilePostItem from './ProfilePostItem';
import PropTypes from 'prop-types';

const ProfilePostList = (props) => {
    let PostItem;
    if (props.postData !== undefined){
        PostItem = props.postData.map((post, i) => {
            return <ProfilePostItem
                        key={i} 
                        index={i}
                        imgUrl={post.images.low_resolution.url} 
                        likes={post.likes.count} 
                        comments={post.comments.count}
                        type={post.type}
                        showPost={props.showPost}
                        id={post.id}
                    />;
            });
    }

    return(
        <div className="profile-posts__items">
            {PostItem}
        </div>
    );
}

ProfilePostList.propTypes = {
    postData: PropTypes.array,
    showPost: PropTypes.func.isRequired
}

export default ProfilePostList;