import React from 'react';
import ProfilePostItem from './ProfilePostItem';
import PropTypes from 'prop-types';

const SavedPostList = (props) => {
    let PostItem = [];
    if (props.postData !== undefined) {
        props.postData.forEach((post, i) => {
            if(post.bookmarked){
                PostItem.push(
                    <ProfilePostItem
                        key={i}
                        index={i}
                        imgUrl={post.images.low_resolution.url}
                        likes={post.likes.count}
                        comments={post.comments.count}
                        type={post.type}
                        showPost={props.showPost}
                        id={post.id}
                    />
                );
            }
        });
    }

    return (
        <div className="profile-posts__items">
            {(PostItem.length ? PostItem : <p style={{padding: "0 15px"}}>There are currently no saved posts</p>)}
        </div>
    );
}

SavedPostList.propTypes = {
    postData: PropTypes.array,
    showPost: PropTypes.func.isRequired
}

export default SavedPostList;