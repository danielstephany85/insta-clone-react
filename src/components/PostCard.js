import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from './Slider';
import Video from './Video';
import CommentList from './comment-components/CommentList';
import CommentForm from './CommentForm';

class PostCard extends Component {

    constructor(props){
        super(props);
        this.state = {
            showForm: false
        }
    }

    static propTypes = {
        postData: PropTypes.array.isRequired,
        id: PropTypes.string,
        index: PropTypes.number,
        deleteComment: PropTypes.func.isRequired,
        showForm: PropTypes.bool.isRequired,
        addComment: PropTypes.func.isRequired,
        toggleLiked: PropTypes.func.isRequired,
        toggleBookmarked: PropTypes.func.isRequired
    }

    toggleForm = () => {
        this.setState({showForm: !this.state.showForm});
    }

    render = () => {
        let postIndex;
        if (this.props.match !== undefined){
            this.post = this.props.postData.filter((post) => post.id === this.props.match.params.id);
            postIndex = parseInt(this.props.match.params.index, 10);
        }else {
            this.post = this.props.postData.filter((post) => post.id === this.props.id);
            postIndex = this.props.index;
        }

        this.post = this.post[0];

        let container_ratio = (this.post.images.standard_resolution.height / this.post.images.standard_resolution.width) * 100;
        container_ratio = container_ratio+'%';

        let media;
        switch (this.post.type) {
            case 'carousel':
                media = <Slider content={this.post.carousel_media} ratio={container_ratio}/>;
                break;
            case 'video':
                media = <Video isActive='active' videoUrl={this.post.videos.standard_resolution.url} ratio={container_ratio}/>;
                break;
            default:
                media = <div className="post-card-img-box" style={{ backgroundImage: `url(${this.post.images.standard_resolution.url})`, paddingTop: container_ratio}}></div>;
        }

        return (
            <article className='post-card'>
                <header>
                    <img src={this.post.user.profile_picture} alt='test' />
                    <h3>{this.post.user.full_name} <br />{(this.post.location !== null) ? <span className="post-location"> {this.post.location.name} </span> : ''}</h3>
                </header>
                <section className="post-card__img-container">
                    {media}
                </section>
                <section className="post-card-info-container">
                    <section className='post-card-actions'>
                        <span className={"action-icons toggles heart " + (this.post.user_has_liked ? 'active' : '')} onClick={() => { this.props.toggleLiked(postIndex)}}>
                            <i className="fa fa-heart-o" aria-hidden="true"></i>
                            <i className="fa fa-heart" aria-hidden="true"></i>
                        </span>
                        <span className='action-icons' onClick={this.toggleForm}>
                            <i className="fa fa-comment-o" aria-hidden="true"></i>
                        </span>
                        <span className={'action-icons toggles ' + (this.post.bookmarked ? 'active' : '')} onClick={() => { this.props.toggleBookmarked(postIndex)}}>
                            <i className="fa fa-bookmark-o" aria-hidden="true"></i>
                            <i className="fa fa-bookmark" aria-hidden="true"></i>
                        </span>
                        <h3>22 likes</h3>
                    </section>
                    <CommentList parentIndex={postIndex} comments={this.post.comments.comment_list} deleteComment={this.props.deleteComment}/>
                    <section className="post-card-date">
                        {(this.post.location !== null) ? <h4>{this.post.location.name}</h4> : ''}
                    </section>
                    {(this.props.showForm || this.state.showForm) ? <CommentForm parentIndex={postIndex} addComment={this.props.addComment}/> : ''}
                </section>
            </article>
        );
    }
}

export default PostCard;