import React ,{Component} from 'react';
import PropTypes from 'prop-types';
import PostCard from './PostCard';

class PostOverlay extends Component {

    closeOverlay = (e) => {
        if( e.target === this.overlay ){
            this.props.toggleOverlay();
        }
    }

    render = () =>{
        return (
            <div className="post-overlay" ref={(div) => { this.overlay = div }} onClick={this.closeOverlay}> 
                <button className="post-overlay__close" onClick={this.props.toggleOverlay}><i className="fa fa-times" aria-hidden="true"></i></button>
                <button className="post-overlay__forward" onClick={() => { this.props.toggleThroughOverlayContent(1)}}><i className="fa fa-chevron-right" aria-hidden="true"></i></button>
                <button className="post-overlay__back" onClick={() => { this.props.toggleThroughOverlayContent(-1) }}><i className="fa fa-chevron-left" aria-hidden="true"></i></button>
                <div className="post-overlay__content"> 
                    <PostCard 
                        postData={this.props.postData} 
                        index={this.props.activeIndex} 
                        id={this.props.id} 
                        deleteComment={this.props.deleteComment} 
                        showForm={true} 
                        addComment={this.props.addComment}
                        toggleLiked={this.props.toggleLiked}
                        toggleBookmarked={this.props.toggleBookmarked}
                    />
                </div>
            </div>
        );
    }
}

PostOverlay.propTypes = {
    toggleOverlay: PropTypes.func.isRequired,
    postData: PropTypes.array.isRequired,
    id: PropTypes.string.isRequired,
    activeIndex: PropTypes.number.isRequired,
    toggleThroughOverlayContent: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    addComment: PropTypes.func.isRequired,
    toggleLiked: PropTypes.func.isRequired,
    toggleBookmarked: PropTypes.func.isRequired
}

export default PostOverlay;