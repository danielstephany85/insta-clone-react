import React, { Component } from 'react';
import PropTypes from 'prop-types'
import Video from './Video'; 

class Slider extends Component {

    constructor(props){
        super(props);
        this.state = {
            activeIndex: 0
        }
    }

    static propTypes = {
        content: PropTypes.array.isRequired,
        ratio: PropTypes.string.isRequired
    }

    componentWillMount = () => {
        this.props.content.forEach(item => {
            if (item.type === 'image'){
                let temp_img = document.createElement('img');
                temp_img.src = item.images.standard_resolution.url;
            }else {
                let temp_video = document.createElement('video');
                temp_video.src = item.videos.standard_resolution.url;
            }

        });
    }

    toggleSlide = (moveValue) => {
        this.setState({activeIndex: (this.state.activeIndex + moveValue)});
    }

    render = () => {
        return(
            <div className="slider-component" style={{ paddingTop: this.props.ratio }}>
                {(this.state.activeIndex === 0) ? '' : <button className="slider-component__back" onClick={()=>{this.toggleSlide(-1)}}><i className="fa fa-arrow-circle-left" aria-hidden="true"></i></button>}
                {(this.state.activeIndex === (this.props.content.length - 1)) ? '' : <button className="slider-component__forward" onClick={()=>{this.toggleSlide(1)}}> <i className="fa fa-arrow-circle-right" aria-hidden="true"></i></button>}
                <div className="slider-component__slides">
                    {this.props.content.map((content_item, i) => {
                            let isActive = (this.state.activeIndex === i) ? 'active' : '';
                            if (this.state.activeIndex === i){
                                if (content_item.type === 'image') {
                                    return <div key={i} className={`slider-component__slide ${isActive}`} style={{ backgroundImage: `url(${content_item.images.standard_resolution.url})`, paddingTop: this.props.ratio }}></div>;
                                } else if (content_item.type === 'video') {
                                    return <Video key={i} videoUrl={content_item.videos.standard_resolution.url} isActive={isActive} ratio={this.props.ratio}/>;
                                }
                                return false;
                            }
                            return false;
                        }
                    )}
                </div>
                <ul className="slider-component__indicators">
                    {this.props.content.map((content_item, i)=> {
                        return <li key={i} className={(this.state.activeIndex === i) ? 'active' : ''}></li>
                    })}
                </ul>
            </div>
        );
    }
}

export default Slider;