import React from 'react';
import { Route, NavLink } from 'react-router-dom'
import PropTypes from 'prop-types';
import ProfilePostList from './ProfilePostList';
import SavedPostList from './SavedPostList';

const PostViewContainer = (props) => {
    
    return (
        <div className="profile-posts-list">
            <div className="profile-posts__selectors">
                <NavLink activeClassName="active" exact to={`${props.match.path}`}><span>POSTS</span><i className="fa fa-th" aria-hidden="true"></i></NavLink>
                <NavLink activeClassName="active" exact to={`${props.match.path}/saved`}><span>SAVED</span><i className="fa fa-bookmark-o" aria-hidden="true"></i></NavLink>
            </div>
            <Route exact path={`${props.match.path}`} render={() => <ProfilePostList showPost={props.showPost} postData={props.postData} />} />
            <Route exact path={`${props.match.path}/saved`} render={() => <SavedPostList showPost={props.showPost} postData={props.postData} />} />
        </div>
    );
}

PostViewContainer.propTypes = {
    postData: PropTypes.array,
    showPost: PropTypes.func.isRequired
}

export default PostViewContainer;