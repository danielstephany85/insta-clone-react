import React from 'react';
import PropTypes from 'prop-types';

const ProfileInfo = (props) => {

    return (
        <div className="profile-info">
            <div className="profile-info__img">
                <img src={props.userData.profile_picture} alt="test-img"/>
            </div>
            <div className="profile-info__stats">
                <div>
                    <h1>{props.userData.username}</h1>
                    {/* <a className="simple-btn">Edit Profile</a>
                    <a className="nav-icon" href="/test.com"><i className="fa fa-cog" aria-hidden="true"></i></a> */}
                </div>
                <div>
                    <ul>
                        <li><b>{props.userData.counts.media}</b> posts</li>
                        <li><b>{props.userData.counts.followed_by}</b> followers</li>
                        <li><b>{props.userData.counts.follows}</b> following</li>
                    </ul>
                </div>
                <div>
                    <p><b>{props.userData.full_name}</b> {props.userData.bio}<br /><a href={props.userData.website} target="_blank">{props.userData.website}</a></p>
                </div>
            </div>
            {/* <div className="profile-info__mobile-blurb">
                <p>
                    <b>{props.userData.full_name}</b> {props.userData.bio}<br />
                    <a href={props.userData.website} target="_blank">{props.userData.website}</a>
                </p>
            </div> */}
            <div className="profile-info__mobile-stats">
                <ul>
                    <li>
                        <b>{props.userData.counts.media}</b><br /><span>posts</span>
                    </li>
                    <li>
                        <b>{props.userData.counts.followed_by}</b><br /><span>followers</span> 
                    </li>
                    <li>
                        <b>{props.userData.counts.follows}</b><br /><span>following</span> 
                    </li>
                </ul>
            </div>
        </div>
    );
}


ProfileInfo.propTypes = {
    userData: PropTypes.object
}
export default ProfileInfo;