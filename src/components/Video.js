import React, { Component } from 'react'
import PropTypes from 'prop-types';

class Video extends Component {

    constructor(props){
        super(props);
        this.state = {
            playing: false
        }
    }

    static propTypes = {
        videoUrl: PropTypes.string.isRequired,
        isActive: PropTypes.string.isRequired,
        ratio: PropTypes.string.isRequired
    }

    toggleVideo = () => {
        if(this.state.playing === false){
            this.setState({playing: true});
            this.video.play(); 
        }else {
            this.video.pause();
            this.setState({playing: false});
        }
    }

    render = () => {
        let isPlaying = this.state.playing ? "playing" : "";
        let isActive = (this.props.isActive === 'active') ? "active" : "";

        return(
            <div className={`video-component ${isActive} ${isPlaying}`} onClick={this.toggleVideo} style={{ paddingTop: this.props.ratio }}>
                <video ref={(video) => { this.video = video }} src={this.props.videoUrl} type="video/mp4" loop playsInline >
                </video>
            </div>
        );
    }
}

export default Video;