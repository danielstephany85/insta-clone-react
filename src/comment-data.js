const comments = [
    {
        id: "testid1",
        comment: "This is a placeholder comment unfortinatly instagrams api does not give access to comments. Below is a little hipster ipsum",
        posted_by: "Daniel Stephany"
    },
    {
        id: "testid2",
        comment: "Lorem ipsum dolor amet pok pok ramps organic cornhole portland synth tumblr kickstarter tbh.",
        posted_by: "Morty Smith"
    },
    {
        id: "testid3",
        comment: "Jianbing yr kombucha vice drinking vinegar thundercats hot chicken.",
        posted_by: "Rick Sanchez"
    },
];

export default comments;