import React, { Component } from 'react';
import {
  BrowserRouter,
  Switch,
  Route
} from 'react-router-dom'
import accessToken from './keys';
import comments from './comment-data';
import MainNav from './components/MainNav';
import PostOverlay from './components/PostOverlay';
import UserProfile from './components/UserProfile';
import PostCard from './components/PostCard'; 


class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      user: undefined,
      recentPosts: undefined,
      showOverlay: false,
      activeId: undefined
    }
  }

  componentDidMount = () => {
    this.getCurrentUser();
    this.getRecentPosts();
  }

  getCurrentUser = () => {
    fetch(`https://api.instagram.com/v1/users/self/?access_token=${accessToken}`)
      .then(res => res.json())
      .then(res => {
        this.isLoading = false;
        this.setState({
          user: res.data
        });
      })
      .catch(err => {
        console.log('there was and error fetching current user content: ' + err);
      });
  }

  getRecentPosts = () => {
    fetch(`https://api.instagram.com/v1/users/self/media/recent/?access_token=${accessToken}&count=18`)
      .then(res => res.json())
      .then(res => {
        return res.data.map((item) => {
          item.comments.comment_list = comments;
          item.bookmarked = false;
          return item;
        })
      })
      .then(res => {
        // console.log(res);
        this.isLoading = false;
        this.setState({
          recentPosts: res
        });
      })
      .catch(err => {
        console.log('there was and error fetching recent posts content: ' + err);
      });
  }

  toggleOverlay = (activeId = undefined, index) => {
    if (this.state.showOverlay === false){
      this.setState({
        showOverlay: true,
        activeId: activeId,
        activePostIndex: index
      });
    }else {
      this.setState({ showOverlay: false });
    }
  }

  toggleThroughOverlayContent = (step) => {
    if (this.state.activePostIndex !== 0 && step === -1){
      this.setState({
        activeId: this.state.recentPosts[(this.state.activePostIndex + step)].id,
        activePostIndex: this.state.activePostIndex + step
      });
    }else if (this.state.activePostIndex !== (this.state.recentPosts.length-1) && step === 1){
      this.setState({
        activeId: this.state.recentPosts[(this.state.activePostIndex + step)].id,
        activePostIndex: this.state.activePostIndex + step
      });
    }
  }

  deleteComment = (index, postId) =>{
    const UpdatedPosts = this.state.recentPosts.map((post,i)=>{
      if(i === index){
        let newCommentList = [];
        post.comments.comment_list.forEach((comment)=> {
          if (postId !== comment.id) newCommentList.push(comment);
        });
        post.comments.comment_list = newCommentList;
      }
      return post;
    });
    this.setState({recentPosts: UpdatedPosts});
  }

  addComment = (index, comment) => {
    const UpdatedPosts = this.state.recentPosts.map((post, i) => {
      if (i === index) {
        post.comments.comment_list.push(comment);
      }
      return post;
    });
    this.setState({ recentPosts: UpdatedPosts });
  }

  toggleLiked = (index) => {
    const UpdatedPosts = this.state.recentPosts.map((post,i)=>{
      if(index === i){
        post.user_has_liked = !post.user_has_liked;
      }
      return post;
    });
    this.setState({recentPosts: UpdatedPosts});
  }

  toggleBookmarked = (index) => {
    console.log(index);
    const UpdatedPosts = this.state.recentPosts.map((post, i) => {
      if (index === i) {
        post.bookmarked = !post.bookmarked;
        console.log(post);
      }
      return post;
    });
    this.setState({ recentPosts: UpdatedPosts });
  }

  render() {
    return (
      <div className="site-constraint">
        
        <BrowserRouter>
          <div>
            <Route path={'/'} render={(props) => {
                let isRoot = false;
              if (props.location.pathname === '/posts' || props.location.pathname === '/posts/saved') {
                  isRoot = true;
                }
                return <MainNav {...props} isRoot={isRoot}/>;
              }
            }/>
            <Switch>
              {/* i had to pass props to the render function then pass the unpacked props to the component to get history */}
              <Route
                path={'/posts'}
                render={(props) =>
                  <UserProfile
                    {...props}
                    userData={this.state.user}
                    postData={this.state.recentPosts}
                    toggleOverlay={this.toggleOverlay}
                  />
                }
              />
              <Route
                path={'/saved'}
                render={(props) =>
                  <UserProfile
                    {...props}
                    userData={this.state.user}
                    postData={this.state.recentPosts}
                    toggleOverlay={this.toggleOverlay}
                  />
                }
              />
              <Route
                path={'/post/:id/:index'}
                render={(props) => (this.state.recentPosts !== undefined)
                  ?
                  <PostCard
                    {...props}
                    postData={this.state.recentPosts}
                    deleteComment={this.deleteComment}
                    showForm={false}
                    addComment={this.addComment}
                    toggleLiked={this.toggleLiked}
                    toggleBookmarked={this.toggleBookmarked}
                  />
                  :
                  <p>Loading...</p>}
              />
            </Switch>
          </div>
        </BrowserRouter>

        {
          (this.state.showOverlay && this.state.activeId !== undefined) 
          ? 
            <PostOverlay 
              toggleOverlay={this.toggleOverlay} 
              postData={this.state.recentPosts} 
              id={this.state.activeId} 
              activeIndex={this.state.activePostIndex}
              toggleThroughOverlayContent={this.toggleThroughOverlayContent}
              deleteComment={this.deleteComment}
              addComment={this.addComment}
              toggleLiked={this.toggleLiked}
              toggleBookmarked={this.toggleBookmarked}
            /> 
          : 
            ''
        }
      </div>
    );
  }
}

export default App;
